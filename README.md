​
### 程序演示效果

![输入图片说明](https://images.gitee.com/uploads/images/2021/0628/111611_3a249620_1335178.png "微信截图_20210628111600.png")

### 关键代码刨析

- 1、关于canvas导入图片

> 导入图片时出现了，canvas加载图片需要二次刷新的问题解决如下

```
var imgurl = $(this).find('img').attr('src');
var img = new Image()
img.crossOrigin = '*'
img.src = imgurl;
if (img.complete) { // 如果图片已经存在于浏览器缓存，直接调用回调函数
    ctx.drawImage(img, 0, 0, $(window).width() * 0.8 - 10, $(window).height() - 140);
    return; // 直接返回，不用再处理onload事件
}
img.onload = function() {
    ctx.drawImage(img, 0, 0, $(window).width() * 0.8 - 10, $(window).height() - 140);
};
```

- 2、撤回功能实现

> 功能流程：在每次操作时使用getImageData方法将每次操作存入数组， 撤回时删除当前展示上一个使用putImageData方法


```
//arr:数组
//ctx:canvas对象
arr.pop();
ctx.clearRect(0, 0, $(window).width() * 0.8 - 10, $(window).height() - 140);
if (arr.length > 0) {
	ctx.putImageData(arr[arr.length - 1], 0, 0, 0, 0, $(window).width() * 0.8 - 10, $(window).height() - 140);
}
```

- 3、橡皮擦部分

> 橡皮跟画线操作可放入同一个方法,橡皮操作使用clearRect方法可以通过参数调整橡皮的像素单位


```
ctx.clearRect(x - 5, y - 5, 10, 10);

```

​